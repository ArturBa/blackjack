#pragma once
#include <iostream>
#include <vector>
#include "Card.h"

class Deck{
  private:
    std::vector<Card> deck;

    friend class Blackjack;
    friend class Player;
  protected:
    void shuffle();
    Card give_card();
    Deck();
  public:
    ~Deck();
    void show();
};
