filetype plugin indent on
syntax on

setlocal spell spelllang=en,pl "spell check
set hlsearch "highlight search
set incsearch "show matches
set ignorecase "ignoring case while searching
set smartcase "unless you use different case
set autoindent
set smartindent
"set textwidth=80
set tabstop=2 "show existing tab with 2 spaces width
set shiftwidth=2 "when indenting with '>', use 2 spaces width
set expandtab "On pressing tab, insert 2 spaces
set title "change window title
set undolevels=25 "set levels of undo

colorscheme smyck "-dark "color settings
set background=dark
set showmatch "showing matching {} etc.
set foldenable "enable folding
set foldmethod=indent "folding based on indent
set nu "numbered lines

"unset arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>
