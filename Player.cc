#include "Player.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

Player::Player(std::string name_buffer, Deck* deck_buffer){
  name=name_buffer;
  card_number=score_calc=0;
  deck_ptr=deck_buffer;
  this->add_card();
  this->add_card();
}
Player::~Player(){
  ;
}
void Player::add_card(){
  cards.push_back(deck_ptr->give_card());
  card_number++;
  score_calc+=cards[cards.size()-1].value;
}
void Player::play(){
  bool game=true;
  while(game){
    this->show_cards();
    game=this->menu();
    system("clear");
  }
}
char Player::score(){
  return score_calc;
}
std::string Player::get_name(){
  return name;
}
void Player::show_cards(){
  for(int i=0; i<card_number; i++)
    cards[i].show_card();
}
bool Player::menu(){
  char buffer, selection;
  do{
    std::cout<<"SCORE:  "<< (int)score_calc<<"\nH: hit 1 card\nP: pass\nSelect action: ";
    std::cin>>selection;
    while((buffer=getchar())!='\n');
    switch(tolower(selection)){
      case 'h':
        this->add_card();
        return true;
      case 'p':
        return false;
      default:
        break;
    }
  }while(true);
}
