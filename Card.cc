#include "Card.h"
#include <string.h>
#include <iomanip>
using namespace std;

Card::Card(int symbol_i, int no){
  set_symbol(symbol_i);
  set_name(no);
} 

void Card::set_symbol(int symbol_i){
  switch(symbol_i){
    case 0:
      symbol="♠"; break;
    case 1:
      symbol="♦"; break;
    case 2:
      symbol="♥"; break;
    case 3:
      symbol="♣"; break;
  }
}
void Card::set_name(int no){
  switch(no){
    case 11:
      strcpy(name, "J"), value=2; break;
    case 12:
      strcpy(name, "Q"), value=3; break;
    case 13:
      strcpy(name, "K"), value=4; break;
    case 14:
      strcpy(name, "As"), value=11; break;
    case 10:
      strcpy(name, "10"), value=10; break;
    default:
      name[0]='0'+no, name[1]=' ', value=no; break;
  }
}

Card::~Card(){
  ;
}

char Card::get_value(){
  return value;
}

void Card::show_card(){
  cout<<"┌─────────┐\n";
  cout<<"│ "<<setw(2);cout<<name<<"      │\n";
  cout<<"│         │\n";
  cout<<"│         │\n";
  cout<<"│    "<<symbol<<"    │\n";
  cout<<"│         │\n";
  cout<<"│         │\n";
  cout<<"│      "<<setw(2);cout<<name<<" │\n";
  cout<<"└─────────┘\n";
}
