#pragma once
#include <iostream>

using namespace std;
class Card{
  private:
    std::string symbol;
    char value;
    char name[3];
    void set_symbol(int);
    void set_name(int);

    friend class Deck;
    friend class Player;
  protected:
    char get_value();
    void show_card();
    Card(int, int);
  public:
    ~Card();
};
