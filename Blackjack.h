#pragma once
#include <iostream>
#include <vector>
#include <stdio.h>
#include "Card.h"
#include "Deck.h"
#include "Player.h"

class Blackjack{
  private:
    Deck deck;
    std::vector<Player> players;
    int no_players;
    bool game;
    void start_screen();
    void logo();
    void call_gamer(int);
    void game_main();
    void results();
  public:
    void play();
    Blackjack();
    ~Blackjack();
};
