#include "Deck.h"
#include <stdlib.h>
#include <time.h>

Deck::Deck(){
  for(int i=0;i<4;i++){
    for(int j=2;j<15;j++){
      deck.push_back(Card(i,j));
    }
  }
}
Deck::~Deck(){
  ;
}
void Deck::shuffle(){
  int shuffles;
  srand(time(NULL));
  while((shuffles=rand()%100+100)<125);

  for(int i=0; i<shuffles; i++){
    int k=rand()%deck.size();
    Card buffor=deck[k];
    deck.erase(deck.begin()+k);
    deck.push_back(buffor);
  }
  //deck.shrink_to_fit();
}
Card Deck::give_card(){
  Card buffer=deck.back();
  deck.pop_back();
  return buffer;
}
void Deck::show(){
  for(int i=0;i<52;i++){
    deck[i].show_card();
  }
}
