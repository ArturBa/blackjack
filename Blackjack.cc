#include "Blackjack.h"
#include <stdlib.h>
#include <iomanip>

Blackjack::Blackjack(){
  ;
}
Blackjack::~Blackjack(){
  ;
}
void Blackjack::play(){
  deck.shuffle();
  this->start_screen();
  this->game_main();
  this->results();
}

/*
void Blackjack::logo(){
  printf(" _____                   __                \n
/\  __`\                /\ \               \n
\ \ \/\ \    ___   ____ \ \ \/'\     ___   \n
 \ \ \ \ \  /'___\/\_ ,`\\ \ , <    / __`\ \n
  \ \ \_\ \/\ \__/\/_/  /_\ \ \\`\ /\ \L\ \ \n
   \ \_____\ \____\ /\____\\ \_\ \_\ \____/\n
    \/_____/\/____/ \/____/ \/_/\/_/\/___/\n");
}
*/
void Blackjack::logo(){
  std::cout<<"logogo\n";
}
void Blackjack::start_screen(){
  this->logo();
  do{
    printf("Type in number of players: ");
    scanf("%d", &no_players);
    printf("Must be less than 13 and more than 2.\n");
  }while(no_players>13||no_players<2);
  //system("clear");
  for(int i=0; i<no_players; i++){
    std::string name_buffer;
    std::cout<<"Player "<<i+1<<" nickname: ";
    std::cin>>name_buffer;
    players.push_back(Player(name_buffer, &deck));
  }
  char char_buffer;
  while((char_buffer=getchar())!='\n');
}
void Blackjack::call_gamer(int i){
  char n;
  //system("clear");
  std::cout<<"It's "<<players[i].get_name()<<" turn."<<std::endl;
  while((n=getchar())!='\n');
}
void Blackjack::game_main(){
  for(int i=0; i<no_players; i++){
    this->call_gamer(i);
    players[i].play();
  }
}
void Blackjack::results(){
  for(int i=0; i<no_players; i++){
    cout<<setw(20)<<players[i].get_name()<<setw(3)<< (int) players[i].score()<<endl;
  }
}

