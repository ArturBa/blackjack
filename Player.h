#pragma once
#include <iostream>
#include <vector>
#include "Card.h"
#include "Deck.h"

using namespace std;
class Player{
  private:
    std::vector<Card> cards;
    std::string name;
    Deck* deck_ptr;
    char card_number;
    char score_calc;

    friend class Blackjack;

    void show_cards();
    bool menu();
    void add_card();
  protected:
    std::string get_name();
    void play();
    char score();
    Player(std::string, Deck*);
  public:
    ~Player();
};
